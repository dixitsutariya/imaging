package com.dixit.imaging.data.data;

import java.util.List;

/**
 * Search user result.
 * */

public class SearchUsersResult {

    public int total;
    public int total_pages;

    public List<User> results;
}
