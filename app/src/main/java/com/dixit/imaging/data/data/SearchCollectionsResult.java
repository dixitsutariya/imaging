package com.dixit.imaging.data.data;

import java.util.List;

/**
 * Search collections result.
 * */

public class SearchCollectionsResult {

    public int total;
    public int total_pages;

    public List<Collection> results;
}
