package com.dixit.imaging.data.data;

import java.util.List;

/**
 * Search photos result.
 * */

public class SearchPhotosResult {

    public int total;
    public int total_pages;

    public List<Photo> results;
}
