package com.dixit.imaging.util;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.widget.ImageView;

import com.dixit.imaging.R;


/**
 * Created by Dixit on 10/14/2017.
 */

public class Loader extends android.app.Dialog {

    public Loader(Context context) {

        super(context, R.style.Theme_AppCompat_DayNight_Dialog_MinWidth);
        this.setContentView(R.layout.loader);
        this.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        this.setCancelable(false);

    }
}
